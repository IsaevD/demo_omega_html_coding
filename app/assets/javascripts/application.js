// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(function(){
    initializeDotazForm();
    initializeFeedbackForm();
    initializePopup();
    initializeFakeSelect();
    initializeMatraceForm();
    initializeProductCard();
});

function initializeDotazForm() {

    searchForm = $("#dotaz_popup form");
    searchTextField = $(searchForm.find("input[type=text]"));
    placeholderClass = ".placeholder";

    searchTextField.focusin(function(){
        $(this).prev(placeholderClass).hide();
    });
    searchTextField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
    });
    searchTextField.prev(placeholderClass).click(function(){
        $(this).next().focus();
    });

}

function initializeMatraceForm() {

    searchForm = $("#matrace_popup form");
    searchTextField = $(searchForm.find("input[type=text]"));
    placeholderClass = ".placeholder";

    searchTextField.focusin(function(){
        $(this).prev(placeholderClass).hide();
    });
    searchTextField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
    });
    searchTextField.prev(placeholderClass).click(function(){
        $(this).next().focus();
    });

}

function initializeFeedbackForm() {

    searchForm = $("#contacts form");
    searchTextField = $(searchForm.find("input[type=text]"));
    searchTextareaField = $(searchForm.find("textarea"));
    placeholderClass = ".placeholder";

    searchTextField.focusin(function(){
        $(this).prev(placeholderClass).hide();
    });
    searchTextField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
    });
    searchTextField.prev(placeholderClass).click(function(){
        $(this).next().focus();
    });

    searchTextareaField.focusin(function(){
        $(this).prev(placeholderClass).hide();
    });
    searchTextareaField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
    });
    searchTextareaField.prev(placeholderClass).click(function(){
        $(this).next().focus();
    });

}

function initializePopup() {

    $(".popup").click(function(){
        $(this).hide();
    });

    $(".popup .popup_cnt").click(function(e){
        e.stopPropagation();
    });

    $(".close_cross").click(function(){
        $(".popup").hide();
    });

    $("header .button, footer .button").click(function(){
        $("body").scrollTop(0);
        $("#dotaz_popup").show();
        return false;
    });

    $("#stars_banner .button").click(function(){
        $("body").scrollTop(0);
        $("#dotaz_popup").show();
        return false;
    });


    $("#products .button_4").click(function(){
        $("body").scrollTop(0);
        $("#matrace_popup").show();
        return false;
    });

    $("#obj_popup .button").click(function(){
        $("#obj_popup").hide();
    });

    $("#matrace_popup form").submit(function(){
        $("#matrace_popup").hide();
        $("#obj_popup").show();
        return false;
    });

    $(".phone .icon").click(function(){
        $("#dotaz_popup").show();
    });

}

function initializeFakeSelect() {
    $("body").click(function(){
        $(".select_field ul.items").hide();
    });
    $(".select_field .fake_select_box").click(function(e){
        $(".select_field ul.items").hide();
        $(this).next().show();
        e.stopPropagation();
    });
    $(".select_field ul.items li").click(function(e) {
        $(this).parent().prev().find(".label").text($(this).text());
        $(this).parent().hide();
        e.stopPropagation();
    });
}

function initializeProductCard() {
    $("#products .link").click(function(){
        obj = $(this).parent().next();
        if ($("body").width() < 1140) {
            obj =  $(obj).parent().find("table.mobile");
        }
        if($(obj).hasClass("hidden")) {
            $(this).children(".icon").removeClass("arrow_bottom_icon").addClass("arrow_top_icon");
            $(obj).find(".hide").show();
            $(obj).removeClass("hidden");
        } else {
            $(this).children(".icon").removeClass("arrow_top_icon").addClass("arrow_bottom_icon");
            $(obj).find(".hide").hide();
            $(obj).addClass("hidden");
        }
        return false;
    });
}


